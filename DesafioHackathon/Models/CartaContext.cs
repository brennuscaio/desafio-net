﻿using Microsoft.EntityFrameworkCore;

namespace DesafioHackathon.Models
{
    public class CartaContext : DbContext
    {
        public CartaContext(DbContextOptions<CartaContext> options) : base(options)
        {
        }
        public DbSet<Carta> Carta { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.ApplyConfiguration(new CartaMap());
            builder.Entity<Carta>().HasKey(m => m.Id);
            base.OnModelCreating(builder);
        }
    }
}
