﻿namespace DesafioHackathon.Models
{
    public class Carta
    {
        public int Id { get; set; }

        public string Nome { get; set; }
        public int Numero { get; set; }
        public string Endere { get; set; }
        public string Dest { get; set; }
        public string Remet { get; set; }      
        public string Mensagem { get; set; }
    }
}
