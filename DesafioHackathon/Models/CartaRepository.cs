﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioHackathon.Models
{
    public class CartaRepository
    {
        private readonly CartaContext _cartaContext;

        public CartaRepository(CartaContext cartaContext)
        {
            _cartaContext = cartaContext;
        }

        public async Task<IEnumerable<Carta>> ObterCarta()
        {
            return await _cartaContext.Carta.ToListAsync();
        }
    }
}
