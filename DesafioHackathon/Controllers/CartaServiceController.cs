﻿using DesafioHackathon.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioHackathon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartaServiceController : ControllerBase
    {
       

        //GET: api/CartaService
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        // GET: api/CartaService/5
        [HttpGet("{id}", Name = "Get")]
        public int Get(int id)
        {
            return id;
        }

        // POST: api/CartaService
        [HttpPost]
        public void Post(Carta carta)
        {

        }

        // PUT: api/CartaService/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
